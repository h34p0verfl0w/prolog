% Dana jest lista punktów:
%[[3,6],[1,2],[5,5],[5,2]]
%-predykat odleglosc(P1,P2,Odl) - euklidesowa odległość między punktami
% -dla każdego punktu znaleźć minimalną odległość do sąsiada - predykat
% minodl(Lista,Punkt,Minodl)
%-posortować według tych odległości
%[[3, 6], [5, 5], [5, 2], [1, 2]]

odleglosc([X1,Y1],[X2,Y2],Odl) :- Odl is sqrt((X1-X2)^2 + (Y1-Y2)^2).

minodl([P|T],P,Minodl) :- minodl(T,P,Minodl).
minodl([H|T],P,Minodl) :- odleglosc(H,P,Odl),
	minodl(T,P,Odl,Minodl).

minodl([],_,Odl,Odl).
minodl([H|T],P,Acc,Wynik) :- odleglosc(H,P,Odl),
	Odl>0,
	Odl < Acc,
	minodl(T,P,Odl,Wynik).
minodl([H|T],P,Acc,Wynik) :- minodl(T,P,Acc,Wynik).

dodajwsp(Lwe,Lwy) :- dodajwsp1(Lwe,Lwe,Lwy).

dodajwsp1([],_,[]).
dodajwsp1([H|T],Lwe,[[Wsp|H]|TT]) :- minodl(Lwe,H,Wsp),
	dodajwsp1(T,Lwe,TT).



bsort(Lwe,Lwy) :- swap(Lwe,Lwe1),
	bsort(Lwe1,Lwy).
bsort(Lwy,Lwy).

swap([[X|K1],[Y|K2]|T],[[Y|K2],[X|K1]|T]) :- X > Y.
swap([Z|T],[Z|TT]) :- swap(T,TT).

usunpierwsze([],[]).
usunpierwsze([[H|K]|T],[K|TT]) :- usunpierwsze(T,TT).

wynik(Lwe,Lwy) :- dodajwsp(Lwe,Lwe1),
	bsort(Lwe1,Lwe2),
	usunpierwsze(Lwe2,Lwy).
	%ZAD 1
	alfabet([a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z]).

	idx(L,O):-alfabet(N),idx(L,N,O).
	idx(H,[H|_],0).
	idx(L,[H|T],I):-L \= H, idx(L,T,I1), I is I1 + 1,!.

	odl(A,B,W):-idx(A,W1),idx(B,W2),W1>W2, W is W1 - W2,!.
	odl(A,B,W):-idx(A,W1),idx(B,W2),W is W2 - W1,!.

	min(A,[B],X):-A\=B,odl(A,B,X).
	min(A,[B,A],I):-odl(A,B,I).
	min(A,[A|T],I):-min(A,T,I).
	min(A,[H|T],I3):-A \= H, odl(A,H,I1),min(A,T,I),I1 > I, I3 is I,!.
	min(A,[H|T],I3):-A \= H, odl(A,H,I1),min(A,T,_),I3 is I1.

	wynik2([],_,[]).
	wynik2([H|T],N,[H1|T1]):-min(H,N,H1),wynik2(T,N,T1),!.

	przestaw([X],[X]).
	przestaw([[H|X],[H1|Y]|T],L2):-H=<H1,przestaw([[H1|Y]|T],L1),L2=[[H|X]|L1].
	przestaw([[H|X],[H1|Y]|T],L2):-H>H1,przestaw([[H1|Y],[H|X]|T],L2).

	bubblesort(In,L2):-przestaw(In,L3),In==L3,L3=L2.
	bubblesort(In,L2):-przestaw(In,L3),In\=L3,bubblesort(L3,L2).

	addhead([],[],[]).
	addhead([H|T],[H1|T1],[H2|T2]):-append([H1],[H],H2),addhead(T,T1,T2).

	cuthead([],[]).
	cuthead([[_|[X]]|T],[X|T1]):-cuthead(T,T1).

	rozw(Tab,Out):-wynik2(Tab,Tab,Wyn),addhead(Tab,Wyn,Zglowami),bubblesort(Zglowami,W),cuthead(W,Out).

	%ZAD 2
	num2bin(1,[1]).
	num2bin(X,Wy):-X mod 2 =:= 1,Y is X //2,num2bin(Y,Wy1),append(Wy1,[1],Wy),!.
	num2bin(X,Wy):-X mod 2 =:= 0,Y is X //2,num2bin(Y,Wy1),append(Wy1,[0],Wy),!.

	polacz([_|[]],[1]):-!.
	polacz([H,X|T],[1|T1]):-H\=X,polacz([X|T],T1),!.
	polacz([H,H|T],[X|T1]):-polacz([H|T],[Y|T1]),X is Y +1.

	zlep([X],[X]):-!.
	zlep([H,H1|T],X):-Y is H*10+H1,zlep([Y|T],X).

	zrob(We,Wy):-num2bin(We,A),polacz(A,B),zlep(B,Wy).

	addhead([],[]).
	addhead([H|T],[[H1|H]|T1]):-zrob(H,H1),addhead(T,T1).

	przestaw([X],[X]).
	przestaw([[H|X],[H1|Y]|T],L2):-H=<H1,przestaw([[H1|Y]|T],L1),L2=[[H|X]|L1].
	przestaw([[H|X],[H1|Y]|T],L2):-H>H1,przestaw([[H1|Y],[H|X]|T],L2).

	bubblesort(In,L2):-przestaw(In,L3),In==L3,L3=L2.
	bubblesort(In,L2):-przestaw(In,L3),In\=L3,bubblesort(L3,L2).

	cuthead([],[]).
	cuthead([[_|H]|T],[H|T1]):-cuthead(T,T1).
	wynik(We,Wy):-addhead(We,A),bubblesort(A,B),cuthead(B,Wy),!.

	%ZAD 4
	dzielniki(_,1,[1]):-!.
	dzielniki(We, Cz, [Cz|T]):-We mod Cz =:= 0,Cz1 is Cz - 1,dzielniki(We,Cz1,T).
	dzielniki(We, Cz, T):-Cz1 is Cz - 1,dzielniki(We,Cz1,T).
	dzielniki(Tablica,Wynik):-T is Tablica - 1,dzielniki(Tablica,T,Wynik),!.

	sumaparz([],0).
	sumaparz([H|We],Wy):-H mod 2 =:= 0,sumaparz(We, Wy1),Wy is Wy1 + H,!.
	sumaparz([_|We],Wy):-sumaparz(We,Wy).

	sumanieparz([],0).
	sumanieparz([H|We],Wy):- \+ H mod 2 =:= 0,sumanieparz(We, Wy1),Wy is Wy1 + H,!.
	sumanieparz([_|We],Wy):-sumanieparz(We,Wy).

	suma(Y,H,X):-Y mod 2 =:= 0,	sumaparz(H,X).
	suma(_,H,X):-sumanieparz(H,X).

	listadzielnikow([],[]).
	listadzielnikow([H|We], [X|Wy]):-dzielniki(H,Dz),suma(H,Dz,X),listadzielnikow(We,Wy),!.

	przestaw([X],[X]).
	przestaw([[H|X],[H1|Y]|T],L2):-H=<H1,przestaw([[H1|Y]|T],L1),L2=[[H|X]|L1].
	przestaw([[H|X],[H1|Y]|T],L2):-H>H1,przestaw([[H1|Y],[H|X]|T],L2).

	bubblesort(In,L2):-przestaw(In,L3),In==L3,L3=L2.
	bubblesort(In,L2):-przestaw(In,L3),In\=L3,bubblesort(L3,L2).

	addheads([],[]).
	addheads([H|T],[X|T1]):-listadzielnikow([H],[Y]),append([Y],[H],X),addheads(T,T1).
	cutheads([],[]).
	cutheads([[_|[TEE]]|T],[TEE|T2]):-cutheads(T,T2).

	odp(We,Wy):-addheads(We,X),bubblesort(X,Y),cutheads(Y,Wy),!.
	my_flatten(X,[X]) :- \+ is_list(X).
my_flatten([],[]).
my_flatten([X|Xs],Zs) :- my_flatten(X,Y), my_flatten(Xs,Ys), append(Y,Ys,Zs).

ile_razy(_,[],0).
ile_razy(X,[H|T],Wynik):-H=X,ile_razy(X,T,Wynik1),Wynik is Wynik1 + 1.
ile_razy(X,[H|T],Wynik):-X\=H,ile_razy(X,T,Wynik1),Wynik is Wynik1.

ile_unikalnych([],_,0).
ile_unikalnych([H|T],Lista,X) :-
  ile_razy(H,Lista,Wyn),
  ile_unikalnych(T,Lista,X1),
  Wyn=0,
  X is X1+1.

ile_unikalnych([H|T],Lista,X) :-
    ile_razy(H,Lista,Wyn),
    Wyn>0,
    ile_unikalnych(T,Lista,X).



stopien_unikalnosci(Lista,Lista1,X):-ile_unikalnych(Lista,Lista1,Z),ile(Lista,Y),X is Z/Y.

trim(X,[X|Tail],Tail).
trim(X,[Y|Tail1],[Y|Tail2]):-trim(X,Tail1,Tail2).



st(Lista1,Lista2,X):-
  trim(Lista1,Lista2,Lista2_trimmed),
  my_flatten(Lista2_trimmed,Lista2_flattened_trimmed),
  stopien_unikalnosci(Lista1,Lista2_flattened_trimmed,X).



bSort(List,Sorted,Lista_list) :- swap(List,List1,Lista_list), !, bSort(List1,Sorted,Lista_list).
bSort(Sorted,Sorted,_).
swap([X,Y|Rest],[Y,X|Rest],Lista_list) :- st(X,Lista_list,XNEW),st(Y,Lista_list,YNEW), XNEW > YNEW.
swap([Z|Rest],[Z|Rest1],Lista_list) :- swap(Rest,Rest1,Lista_list).
pomocnik(0,0).
pomocnik(X,-1):-
  X =< -1.
pomocnik(X,1):-
  X >= 1.

konwersja(/,1).
konwersja(\,-1).

zamianalisty([X],TMP,A,[Z]):-
  konwersja(X,Y),
  G is Y+TMP,
  pomocnik(G,W),
  Z is W+A.

zamianalisty([H|T],TMP,A,[Anew|T1]) :-
  konwersja(H,Y),
  G is Y+TMP,
  pomocnik(G,W),
  Anew is A+W,
  TMPnew is Y,
  zamianalisty(T,TMPnew,Anew,T1).

listfix(In,Out) :- zamianalisty(In,0,0,Out).

sorty(Lista,Sorted):-
  swap(Lista, L1),!,sorty(L1,Sorted).
sorty(X,X).

swap([X,Y|T],[Y,X|T]) :- X>Y.
swap([H|R],[H|R1]) :- swap(R,R1).



usuwanko(X,[_|T],W):-
  X>0,
  X1 is X-1,
  usuwanko(X1,T,W).

usuwanko(0,X,X).



wyznaczwieksz(V1,C1,_,C2,V1,C1):-
  C1>C2.
wyznaczwieksz(_,_,V2,C2,V2,C2).


wyznacztoziomus([H|T],V1,C1,X) :-
  pomozmituziomus([H|T],0,P),
  wyznaczwieksz(V1,C1,H,P,V1New,C1New),
  usuwanko(P,[H|T],W),
  wyznacztoziomus(W,V1New,C1New,X),!.

wyznacztoziomus([],V1,_,V1).


pomozmituziomus([H|T],A,X):-
  [G|_] = T,
  H=G,
  Anew is A+1,
  pomozmituziomus(T,Anew,X).

pomozmituziomus([],A,A).
pomozmituziomus([_],A,X1):-X1 is A+1.
pomozmituziomus([H|T],A,X):-
  [G|_] = T,
  H\=G,
  X is A + 1.



powiedzmiwynik(In,Out) :-
  listfix(In,Out1),
  sorty(Out1,Out2),
  wyznacztoziomus(Out2,0,0,Out).
	Sum([],ACC,ACC).
sum([H|T],ACC,Lout) :- ACC1 is ACC+H,
	sum(T,ACC1,Lout).
sum(Lin,Sum) :- sum(Lin,0,Sum).

count([],0).
count([_|T],Count) :- count(T,C1),
	Count is C1+1.

centers([],0).
centers(LIN, Res) :- sum(LIN,L1),
	count(LIN,L2),
	Res is L1 / L2.

centersLL([],[]).
centersLL([H|T],[Centers|T1]) :- centers(H,Centers),
	centersLL(T,T1).

remove_item(_,[],[]) :- !.
remove_item(X,[X|T],T) :- !.
remove_item(X,[H|T],[H|T1]) :- remove_item(X,T,T1),!.

distances(_,[],[]) :- !.
distances(X,[H|T],[D|T1]) :- abs(X-H,D),
	distances(X,T,T1),!.

min([],ACC,ACC) :- !.
min([H|T],AKU,Min) :- H < AKU,
	min(T,H,Min),!.
min([H|T],AKU,Min) :- H >= AKU,
	min(T,AKU,Min).
min([H|T],Min) :- min(T,H,Min).

min_distance(X,Lx,L) :- remove_item(X,Lx,L1),
	distances(X,L1,L2),
	min(L2,L).

foreach2([],_,[]).
foreach2([KEY|T],DATA,[VALUE|T1]) :- min_distance(KEY, DATA, VALUE),
	foreach2(T,DATA,T1).


foreach(IN,OUT) :- foreach2(IN,IN,OUT).

swap([X],[X]) :- !.
swap([[H|X],[H1|Y]|T],L2) :- H =< H1,
	swap([[H1|Y]|T],L1),
	L2 = [[H|X]|L1],!.
swap([[H|X],[H1|Y]|T],L2) :- H > H1,
	swap([[H1|Y],[H|X]|T],L2),!.


bubblesort(Lin, L3) :- swap(Lin,L2),
	Lin == L2,
	L3 = Lin,!.
bubblesort(Lin, L3) :- swap(Lin,L2),
	Lin \= L2,
	bubblesort(L2,L3),!.

cuthead([],[]).
cuthead([[_|H]|T1],[H|T]) :- cuthead(T1,T).

%oblicza min odleglosci srodkow przedzialow
magic(Lin,Lout) :- centersLL(Lin,L1),
	foreach(L1,Lout).

%dokleja do listy odp. ogony, np [1,2,3],[6,7,8] => [1|6],[2|7]
hard([],[],[]).
hard([H|T],[H1|T1],[[H1|H]|T2]) :- hard(T,T1,T2).

%dokleja do ogona przedzialow ich najmniejsze odl
hardx(Lin,Lout) :- magic(Lin,L2),
	hard(Lin,L2,Lout).

final(Lin,Lout) :- hardx(Lin,L2),
	bubblesort(L2,L3),
	cuthead(L3,Lout).
	genList([],_,[]):-!.
genList([H|T],ACC,[H1|T1]) :- H = /,
        ACC1 is ACC+1,
    H1 is ACC1,
	genList(T,ACC1,T1),!.
genList([H|T],ACC,[H1|T1]) :- H = \,
	ACC1 is ACC-1,
	H1 is ACC1,
	genList(T,ACC1,T1),!.
genList(Lin,Lout) :- genList(Lin,1,Lout).

occur(_,[],ACC,ACC):-!.
occur(El,[H|T],ACC,Occ) :- El =:= H,
	ACC1 is ACC+1,
	occur(El,T,ACC1,Occ),!.
occur(El,[H|T],ACC,Occ) :- El =\= H,
	occur(El,T,ACC,Occ),!.
occur(El,L,Occ) :- occur(El,L,0,Occ).

mostCommon([],_,ACC,ACC):-!.
mostCommon([KEY|T], DATA, ACC,Freq) :- occur(KEY,DATA,FreqTemp),
	FreqTemp > ACC,
	ACC1 is KEY,
	mostCommon(T,DATA,ACC1,Freq),!.
mostCommon([KEY|T],DATA,ACC,Freq) :- occur(KEY,DATA,FreqTemp),
	FreqTemp =< ACC,
	mostCommon(T,DATA, ACC, Freq),!.
mostCommon(IN,OUT) :- mostCommon(IN,IN,0,OUT).

final(Lin,Out) :- genList(Lin,L1),
	mostCommon(L1,Out).
	%Common - szuka części wspólnej
%cut common - obcina część wspólną
%unique single list - skrócenie przedziału w oparcie o inne
%unique list list - skrócenie wszystkich przedziałów
%del myself - usuwa siebie z kopii listy początkowej
%width list - lista szerokości przedziałów - OK
%sortowanie z kluczem
%całość w mainie
%część wspólna przedziałów [A,B], [C,D] istnieje jeśli max(A,C)<min(B,D)

width([A,A],0):-!.
width([A,B],X):- X is B-A.

width_list([],[]):-!.
width_list([H|T],[Ha|Ta]):- width(H,Ha),width_list(T,Ta).

max(A,B,X):- A>B, X is A.
max(A,B,X):- A=<B,X is B.

min(A,B,X):- A<B, X is A.
min(A,B,X):- A>=B,X is B.

is_common([A,B],[C,D]):- max(A,C,X),min(B,D,Y),X<Y,!.

common([A,B],[C,D],[X,Y]):- is_common([A,B],[C,D]),max(A,C,X),min(B,D,Y),!.
common([A,B],[C,D],[]).

c_c([],[_,_],[]):-!.
c_c([A,B],[C,D],[Nx,Ny]):- is_common([A,B],[C,D]), common([A,B],[C,D],[X,_]),A<X, Nx is A, Ny is X-1,!.
c_c([A,B],[C,D],[]):- C=<A, D>=B,!.
c_c([A,B],[C,D],[A,B]):-common([A,B],[C,D],[]).


del_myself([A,B],[H|T],T):- H == [A,B],!.
del_myself([A,B],[H|T],[H|X]):- del_myself([A,B],T,X).

uniq_single_b(H,[L],O):- c_c(H,L,O).
uniq_single_b(P,[H|T],O):- c_c(Q,H,O),uniq_single_b(P,T,Q).

uniq_single(P,List,O):-del_myself(P,List,Cut),uniq_single_b(P,Cut,O).

uniq_full([],List,[]).
uniq_full([H|T],List,[Hout|Tout]):-uniq_single(H,List,Hout),uniq_full(T,List,Tout).

make(In,Out):- uniq_full(In,In,C),width_list(C,Out).

addhead([],[]).
addhead([H|T],[[H1|H]|T1]):- make(H,H1),addhead(T,T1).
cuthead([],[]).
cuthead([[_|H]|T1],[H|T]):- cuthead(T1,T).

swap([X],[X]).
swap([H|X],[[H1|Y]|T],L2):- H=<H1,swap([[H1|Y]|T],L1), L2 = [[H|X]|L1].
swap([H|X],[[H1|Y]|T],L2):- swap([[H1|Y],[H|X]|T],L2).

bubblesort(Ln,L3):- swap(Ln,L2), Ln == L2, L3=L2,!.
bubblesort(Ln,L3):- swap(Ln,L2), Ln\=L2, bubblesort(L2,L3).

main(In,Out):- addhead(In,X),bubblesort(X,Y),cuthead(Y,Out).
rozbij(X,_):-X<1,!.
rozbij(X,C):-Y is (X rem 10),W is X//10,rozbij(W,Z),append(Z,[Y],C),!.
rozbij2([],[]).
rozbij2([H|T],[X|Z]):-rozbij(H,X),rozbij2(T,Z).
usun([],[]):-!.
usun([H,H1|T],Z):-H>H1,append([H],T,C),usun(C,Z),!.
usun([H|T],[H|X]):-usun(T,X),!.
usunliste([],[]).
usunliste([H|T],[X|Z]):-usun(H,X),usunliste(T,Z),!.
polacz([X],[X]).
polacz([H,H1|T],X):-W is (H*10+H1),polacz([W|T],X),!.
polaczliste([],[]).
polaczliste([H|T],[X|Z]):-polacz(H,X),polaczliste(T,Z),!.
scal([],[],[]).
scal([H1|T1],[H2|T2],[W|X]):-append(H1,H2,W),scal(T1,T2,X).
arytmetyczny([_]).
arytmetyczny([H,H1|T]):-append([V],_,H),append([V1],_,H1),V=<V1,arytmetyczny([H1|T]).
zamien([],[]).
zamien([H,H1|T],X):-append([V],_,H),append([V1],_,H1),V>V1,zamien([H1,H|T],X),!.
zamien([H|T],[H|Z]):-zamien(T,Z),!.
bubble([],[]).
bubble(L,L):-arytmetyczny(L).
bubble(L,Z):-zamien(L,X),bubble(X,Z),repeat,!.
odetnijglowe([],[]).
odetnijglowe([H|T],[X|Z]):-append([_],X,H),odetnijglowe(T,Z).
wynik(A,B):-rozbij2(A,X),usunliste(X,Y),polaczliste(Y,Z),scal(Z,A,C),bubble(C,V),odetnijglowe(V,B).
% Mamy listę zawierającą współrzędne środków oraz długości boków kwadratów.
% Zadanie polega na zmodyfikowaniu jej w taki sposób, aby żaden z kwadratów
% nie zawierał żadnego innego środka niż swojego własnego.
% I: [[ŚrX0, ŚrY0, Dłg0], [ŚrX1, ŚrY1, Dłg1], ..., [ŚrXn, ŚrYn, Dłgn]]
% O: [[ŚrX0, ŚrY0, Dłg0_2], [ŚrX1, ŚrY1, Dłg1_2], ..., [ŚrXn, ŚrYn, Dłgn_2]]

% Znalezienie przedziałów dla kwadratu.
range([SrX, SrY, L], [[X,Xs],[Y,Ys]]) :- HL is (L/2),
	X is (SrX-HL), Xs is (SrX+HL),
	Y is (SrY-HL), Ys is (SrY+HL).
% range([6,6,10], [[1,11],[1,11]]).
% range([3,4,4], [[1,5],[2,6]]).
% range([4,3,2], [[3,5],[2,4]]).

% Sprawdzenie czy śr. kwadratu znajduje się w innym kwadracie.
check([SrX, SrY, _], [SrXs, SrYs, Ls]) :-
	range([SrXs, SrYs, Ls], [[X,Xs],[Y,Ys]]),
	X < SrX, SrX < Xs,
	Y < SrY, SrY < Ys,
	true, !.
check([SrX, SrY, _], [SrXs, SrYs, Ls]) :-
	range([SrXs, SrYs, Ls], [[X,Xs],[Y,Ys]]),
	X >= SrX, SrX >= Xs,
	Y >= SrY, SrY >= Ys,
	false, !.
% check([3,4,4], [6,6,10]). -> true
% check([4,3,2], [6,6,10]). -> true
% check([4,3,2], [3,4,4]). -> true

% Znalezienie połowy nowej długości
find_max([_,_,L], [], 1, L) :- !.
find_max(_, [], _, _).
find_max([SrX,SrY,_], [[SrXs,SrYs,_]|T], 1, Max) :-
	Temp1 is (SrX-SrXs), Temp1s is abs(Temp1),
	Temp2 is (SrY-SrYs), Temp2s is abs(Temp2),
	maximum(Temp1s, Temp2s, Max),
	find_max([SrX,SrY,_], T, 0, Max).
find_max([SrX,SrY,_], [[SrXs,SrYs,_]|T], 0, Max) :-
	Temp1 is (SrX-SrXs), Temp1s is abs(Temp1),
	Temp2 is (SrY-SrYs), Temp2s is abs(Temp2),
	maximum(Temp1s, Temp2s, Temp),
	Temp > Max, find_max([SrX,SrY,_], T, 0, Temp).
find_max([SrX,SrY,_], [[SrXs,SrYs,_]|T], 0, Max) :-
	Temp1 is (SrX-SrXs), Temp1s is abs(Temp1),
	Temp2 is (SrY-SrYs), Temp2s is abs(Temp2),
	maximum(Temp1s, Temp2s, Temp),
	Temp =< Max, find_max([SrX,SrY,_], T, 0, Max).

maximum(X, Y, X) :- X >= Y, !.
maximum(X, Y, Y) :- Y > X.

% Znalezienie dla kwadratu kwadratów zawierających się w nim.
find(_, [], []).
find(K, [H|T], [H|S]) :- check(H, K), find(K, T, S).
find(K, [H|T], S) :- not(check(H,K)), find(K, T, S).

find_list([], _, []).
find_list([H|T], Lista, [[H,Hs]|S]) :- find(H, Lista, Hs),
	find_list(T, Lista, S).

find_list(In, Out) :- find_list(In, In, Out).

% Usunięcie samego siebie z listy kwadratów zawierających się.
del(_, [], []).
del(El, [El|T], S) :- del(El, T, S).
del(El, [H|T], [H|S]) :- H \= El, del(El, T, S).

del_list([], []).
del_list([[H,Hs]|T], [[H,Hx]|S]) :- del(H, Hs, Hx),
	del_list(T, S).

new_length([], []).
new_length([[H,Hs]|T],[[H,L]|S]) :- find_max(H, Hs, 1, L),
	new_length(T, S).

insert([], []).
insert([[[X,Y,_],L]|T], [[X,Y,L]|S]) :- insert(T, S).

main(In, Out) :- find_list(In, Temp1),
	del_list(Temp1, Temp2),
	new_length(Temp2, Temp3),
	insert(Temp3, Out).
